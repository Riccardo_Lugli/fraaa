//============================================================================
// Name        : Prova_Esame.cpp
// Author      : Riccardo Lugli
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <cstring>
using namespace std;

#include "parola.h"

parola* load(){
	ifstream f;
	f.open("inverted");
	int n,id;
	f>>n;
	parola * v = new parola[n];
	for(int i = 0;i < n;i++){
		f >> v[i].p;
		f >> v[i].n_doc;
		v[i].l = NULL;
		for(int j = 0;j < v[i].n_doc;j++){
			f >> id;
			v[i].l = ord_insert_elem(v[i].l,new_elem(id));
		}
	}
	f.close();
	return v;
}


void stampa(parola * II,int n){
	lista app;
	for(int i = 0;i < n;i++){
		cout<<II[i].p<<endl;
		cout<<II[i].n_doc<<" documenti"<<endl;
		app = II[i].l;
		for(int j = 0;j < II[i].n_doc;j++){
			cout<<app->inf<<" ";
			app = tail(app);
		}
		cout<<endl;
	}
}

void update(parola*  & II,char * file_name,int & n){
	ifstream f;
	f.open(file_name);
	int id;
	bool flag = false;
	char word[80];
	f >> id;
	while(f.good()){
		f >> word;
		flag = false;
		for(int i = 0;i < n;i++){
			if(strcmp(II[i].p,word) == 0){
				II[i].n_doc += 1;
				II[i].l = ord_insert_elem(II[i].l,new_elem(id));
				flag = true;
			}
		}
		if(!flag){
			n += 1;
			parola * p = new parola[n];
			for(int i = 0;i < n-1;i++){
				p[i].n_doc = II[i].n_doc;
				strcpy(p[i].p,II[i].p);
				p[i].l = II[i].l;
			}
			delete [] II;
			p[n-1].n_doc = 1;
			strcpy(p[n-1].p,word);
			p[n-1].l = ord_insert_elem(p[n-1].l,new_elem(id));
			II = new parola[n];
			for(int i = 0;i < n;i++){
				II[i].n_doc = p[i].n_doc;
				strcpy(II[i].p,p[i].p);
				II[i].l = p[i].l;
			}
			delete [] p;
		}
	}

}

void AND(parola* II,char * W1, char * W2,int n){
	lista app = NULL;
	lista app1 = NULL;
	lista app2 = NULL;
	for(int i = 0;i < n;i++){
		app = II[i].l;
		if(strcmp(II[i].p,W1) == 0){
			while(app != NULL){
				if(ord_search(app1,app->inf) == NULL)
					app1 = ord_insert_elem(app1,new_elem(app->inf));
				app = tail(app);
			}
		}
		if(strcmp(II[i].p,W2) == 0){
			while(app != NULL){
				if(ord_search(app2,app->inf) == NULL)
					app2 = ord_insert_elem(app2,new_elem(app->inf));
				app = tail(app);
			}
		}
	}
	while(app1 != NULL && app2 != NULL){
		if(app1 == NULL && app2 != NULL){
			cout<<app2->inf<<" ";
			app2 = tail(app2);
		}
		else if(app2 == NULL && app1 != NULL){
			cout<<app1->inf<<" ";
			app1 = tail(app1);
		}
		else{
			if(ord_search(app1,app2->inf) != NULL)
				cout<<app2->inf<<" ";
			app2 = tail(app2);
			app1 = tail(app1);
		}
	}
	cout<<endl;
}

int* match(parola* II,char ** WL,int n,int n_word){
	lista app = NULL;
	lista app1 = NULL;
	int cont = 0;
	for(int j = 0;j < n_word;j++){
		for(int i = 0;i < n;i++){
			app = II[i].l;
			while(app != NULL){
				if(ord_search(app1,app->inf) == NULL && strcmp(II[i].p,WL[j]) == 0){
						app1 = ord_insert_elem(app1,new_elem(app->inf));
						cont++;
				}
				app = tail(app);
			}
		}
	}
	app = app1;
	int * v1 = new int[cont];
	int * v2 = new int[cont];
	for(int i = 0; i < cont;i++){
		v1[i] = 0;
		v2[i] = app->inf;
		app = tail(app);
	}
	int cont1 = cont;
	lista app2;
	for(int j = 0;j < n_word;j++){
		for(int i = 0;i < n;i++){
			if(strcmp(II[i].p,WL[j]) == 0){
				app = II[i].l;
				while(app != NULL){
					app2 = app1;
					cont = 0;
					while(app2 != NULL){
						if(ord_search(app2,app->inf) != NULL){
							v1[cont]++;
						}
						cont++;
						app2 = tail(app2);
					}
					app = tail(app);
				}
			}
		}
	}
	for (int i=0; i<cont1-1; i++) {
		int index = i;
	    for (int j=i+1; j<cont1; j++)
	    	if (v1[j] < v1[index])
	            index = j;
	      int temp = v2[index];
	      v2[index] = v2[i];
	      v2[i] = temp;
	   }
	for(int i = 0;i < cont1;i++)
		cout<<v2[i]<<" ";
	return v2;
}

int main() {
	ifstream f;
	f.open("inverted");
	int n;

	f >> n;
	parola * v = new parola[n];
	int *v1 = new int[n];
	v = load();
	stampa(v,n);
	update(v,"doc",n);
	stampa(v,n);
	//AND(v,"computer","tower",n);
	char **dio = (char**) malloc(20*sizeof(char));
	dio[0] = new char[20];
	dio[1] = new char[20];
	strcpy(dio[0],"computer");
	strcpy(dio[1],"voltage");
	v1 = match(v,dio,n,2);
	return 0;
}
